const form = document.querySelector('form');
const menu = document.querySelector('select');
const GIPHY_URL =
  'https://api.giphy.com/v1/gifs/translate?api_key=tpaHeLvAj1Vn6nWt9BoClrax0UvLuW81&s=';

menu.addEventListener('change', function(event) {
  event.preventDefault();
  const formData = new FormData(form);
  const adjective = formData.get('name');
  getData(adjective);
});

function getData(searchTerm) {
  console.log(searchTerm);
  fetch(GIPHY_URL)
    .then(function(response) {
      return response.json();
    })
    .then(function(giphyData) {
      console.log(giphyData.data);
    })
    .catch(function(err) {
      console.log(err.message);
    });
}
